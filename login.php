<?php
	$server = $_SERVER['PHP_SELF'];
	session_start();
	//import dependant files
	require_once("setup.php");
	require_once("functions.php");
	//require_once("phpmailer/class.phpmailer.php");
	$ip = $_SERVER["REMOTE_ADDR"];
	$server = $_SERVER['PHP_SELF'];
	$email = $pass = '';
	if(isset($_POST['submit']))
	{
		$email = $_POST['email'];
		$email_hash = NTLMHash($email);
		$pass = $_POST['pass'];
		$hash = NTLMHash($pass);
		//insert entry into attempts table
		$insert_attempt = "INSERT INTO $tbl_attempts (`address` ,`timestamp`,`email`)VALUES ('$ip',CURRENT_TIMESTAMP, '$email')";
		mysqli_query($con, $insert_attempt);
		$result = mysqli_query($con, "SELECT COUNT(*) FROM $tbl_attempts WHERE `address` LIKE '$ip' AND `timestamp` > (now() - interval 10 minute) AND `email` LIKE '$email'");
		$count = mysqli_fetch_array($result, MYSQLI_NUM);

		if($count[0] > 2){
		  echo "
		  <h2>Sorry, We had to shut you down for security purposes</h2>
		  <p class='err_box'>System Access Revoked due to the following reasons:-
		  <ol>
		  	<li>You are trying to bruteforce Login
		  	<li>You are a bot
		  	<li>You are not permitted to access the System
		  	<li>You forgot your password. We all do sometimes <a href=\"reset.php\">Reset Password</a>

		  </ol>
		  </p>
		  <h3>What Next?</h3>
		  <p>Reattempt Again after 10 Minutes only! To reset your password now, follow the link <a href=\"reset.php\">Reset Password</a></p>
		  ";
		  exit;
		}

		//fetch data and check whether it's valid

		$data = fetchData($tbl_users, 'email', $email);
		if($data == 'No Record')
		{
			//No record exist
			echo "<script type='text/javascript'>alert(\"Invalid Login...\")</script>";
		}else
		{
			$access = $data['access'];
			$pass_hash = $data['pass'];
			$status = $data['status'];
			$email_hash = $data['email_hash'];
			$link = "http://192.168.64.2/imis/newuser.php?sessid=$email_hash";
			if($status != 1)
			{
				header("Location: $link");
				die();
			}
			if($hash != $pass_hash)
			{
				echo "<script type='text/javascript'>alert(\"Invalid Login...\")</script>";
			}else
			{
				//Delete user Data
				$delete_sql = "DELETE FROM $tbl_attempts WHERE `address` LIKE '$ip' AND `email` LIKE '$email'";
				mysqli_query($con, $delete_sql) or die("Error Updating Record");
				if($access == 'SUPER ADMIN')
				{
					$_SESSION['superadmin'] = $email;
					header('Location:superuser/');
				}
				if($access == 'ADMINISTRATOR')
				{
					$_SESSION['admin'] = $email;
					header('Location:admin/');
				}
				if($access == 'REGIONAL DIRECTOR')
				{
					$_SESSION['director'] = $email;
					header('Location:user/');
				}
				if($access == 'NORMAL USER')
				{
					$_SESSION['user'] = $email;
					header('Location:user/');
				}

			}

		}
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Login - IMIRMS</title>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<script src='jquery.js'></script>
		<script type='text/javascript' src='script.js'></script>
		<link rel='stylesheet' type='text/css' href='style.css'>
	</head>
	<body>
		<p style='text-align: center;'  id='login_form'>
			<img class='logo' src='images/mining.png'>
		</p>
		<form style='text-align: center;' class='reg_form' action='<?php echo $server;?>' method='post'>
				<p>WELCOME TO IMIRMS PORTAL</p>
				<p><input type='text' autofocus title='Please Enter Valid Email e.g johndoe@domain.com' pattern='[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$' name='email' required placeholder='Email' ></p>
				<p><input type='password' name='pass' required placeholder='Password' ></p>
				<p>
					<input type='submit' name='submit' value='Login'>
				</p>
				<p style='text-align: justify;padding-left: 15px;'>Forgot Password? We All do, <a href="reset.php"> Reset Password</a></p>
				<p style='text-align: justify;padding-left: 15px; font-weight: bold;'>[ Maximum Attempts: 3 ]</p>
				<p style='text-align: justify;padding-left: 15px; color: red; font-style: italic;'>Be Warned: Unauthorised Access to IMIRMS is Prohibited</p>
		</form>
	</body>
</html>