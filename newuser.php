<?php
	session_start();

	//imports
	require_once("setup.php");
	require_once("functions.php");
	//require_once("phpmailer/class.phpmailer.php");
	
	$sessid = $_REQUEST['sessid'];
	$server = $_SERVER['PHP_SELF']."?sessid=$sessid";

	//display Header
	echo "
		<!DOCTYPE html>
		<html>
		<head>
			<title>Login - IMIRMS</title>
			<meta name='viewport' content='width=device-width, initial-scale=1'>
			<script src='jquery.js'></script>
			<script src='strenght.js'></script>
			<script type='text/javascript' src='script.js'></script>
			<link rel='stylesheet' type='text/css' href='style.css'>
			<script type='text/javascript'>
				$(document).ready(function($) {
				$('#myPassword').strength({
				            strengthClass: 'strength',
				            strengthMeterClass: 'strength_meter',
				            strengthButtonClass: 'button_strength',
				            strengthButtonText: 'Show Password',
				            strengthButtonTextToggle: 'Hide Password'
				        });
				});
			</script>
		</head>
	";
	

	
	$email = '';
	$status = 0;
	$field = 'email_hash';


	//check user email
	$data = fetchData($tbl_users, $field, $sessid);

	if($data == 'No Record')
	{
		echo "<p class='err_box'>Warning: Invalid Authorization Code. Copy the link send to your email or contact System Administrator</p>";

	}else
	{

		$email = $data['email'];
		//check if already active

		$status = $data['status'];
		$access = $data['access'];
		if($status == 1)
		{
			//redirect to login page
			//echo "<script type='text/javascript'>alert(\"Your Account Has Already Been Activated. Kindly Login To Access The System\")</script>";
			header('Location:login.php');
		}
		else
		{
			//redirect to account activation page

		
		//receive form data
		if(isset($_POST['submit']))
		{
			$oldpass = $_POST['oldpass'];
			$pass = $_POST['pass'];
			$conpass = $_POST['conpass'];
			$pass_hash = NTLMHash($pass);
			$oldpass_hash = NTLMHash($oldpass);
			if($pass != $conpass)
			{
				echo "<script type='text/javascript'>alert(\"Your Passwords Donot Match\")</script>";
			}else
			{
				//check if password is ok
				$sql_check = "SELECT * FROM $tbl_users WHERE email = '$email' AND pass='$oldpass_hash'";
	    		$isValid = mysqli_query($con, $sql_check);
	    		$data = mysqli_num_rows($isValid);
	    		if($data != 0)
				{
					//UPDATE USER ACCOUNT DETAILS
					$sql_update = "UPDATE users SET pass = '$pass_hash', status ='1' WHERE email ='$email'";
					$isUpdated = mysqli_query($con, $sql_update) or die("Error Updating Your Account");
					if($isUpdated)
					{
						echo "<script type='text/javascript'>alert(\"Congratulations! Your Account is now Active\")</script>";
						while($record = mysqli_fetch_array($isValid))
						{
							extract($record);
							$status = $record['status'];
							$access = $record['access'];
						}
						if($access == 'SUPER ADMIN')
						{
							$_SESSION['superadmin'] = $email;
							header('Location:superuser/');
						}
						if($access == 'ADMINISTRATOR')
						{
							$_SESSION['admin'] = $email;
							header('Location:admin/');
						}
						if($access == 'REGIONAL DIRECTOR')
						{
							$_SESSION['director'] = $email;
							header('Location:user/');
						}
						if($access == 'NORMAL USER')
						{
							$_SESSION['user'] = $email;
							header('Location:user/');
						}
						
					}else
					{
						echo "<script type='text/javascript'>alert(\"Warning: Wrong Password! Please Copy and Paste Your Password\")</script>";
					}
				}
				else
				{
					//Authorized access
					echo "<script type='text/javascript'>alert(\"Your Session Has Expired, Kindly contact System Administrator to resend Activation Email\")</script>";					
				}

			}
		}
		
	}

		echo 
		"
<body>
	<p style='text-align: center;'  id='login_form'>
		<img class='logo' src='images/mining.png'>
	</p>
	<form style='text-align: center;' class='reg_form' action='$server' method='post'>
			<p>WELCOME TO IMIRMS PORTAL</p>
			<p><input type='text' name='email' readonly value='$email' ></p>
			<p><input type='password' name='oldpass' required placeholder='Old Password' title='Paste Your Password Here' ></p>
			<p><input id='myPassword' type='password' title='Enter New Password' name='pass' required placeholder='New Password' ></p>
			<p><input type='password' name='conpass' title='Confirm New Password' required placeholder='Confirm Password' ></p>
			<p>
				<input type='submit' name='submit' value='Activate Account' title='Updates password and logs you in'>
			</p>
			<p style='text-align: justify;padding-left: 15px; color: red; font-style: italic;'>Be Warned: Unauthorised Access to IMIRMS is Prohibited</p>
	</form>
</body>
</html>

		";
	}
?>
