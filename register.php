<?php

	session_start();

	//import dependant files
	require_once("setup.php");
	require_once("functions.php");
	require_once("phpmailer/class.phpmailer.php");
	//Variables
	$server = $_SERVER['PHP_SELF'];
	$fname = '';
	$lname = '';
	$empno = '';
	$phone = '';
	$email = '';
	$pass = randomString();
	$pass_hash = NTLMHash($pass);
	$dept = '';
	$division = '';
	$region = '';
	$access = '';
	$status = 0;
	$added_by = 'VPLANGAT@GMAIL.COM';
	
	
	echo 
	"
		<!DOCTYPE html>
		<html>
		<head>
			<title>Registration - IMIRMS</title>
			<meta name='viewport' content='width=device-width, initial-scale=1'>
			<script src='jquery.js'></script>
			<script type='text/javascript' src='script.js'></script>
			<link rel='stylesheet' type='text/css' href='css/bootstrap.min.css'>
			<link rel='stylesheet' type='text/css' href='style.css'>

		</head>
	";
	if(isset($_SESSION['superadmin']) || isset($_SESSION['admin']) || isset($_SESSION['director']))
	{
		if(isset($_SESSION['superadmin'])){$added_by = $_SESSION['superadmin'];}
		if(isset($_SESSION['admin'])){$added_by = $_SESSION['admin'];}
		if(isset($_SESSION['director'])){$added_by = $_SESSION['director'];}
		//check if form is submitted
		if(isset($_POST['submit']))
		{
			$fname = strtoupper($_POST['fname']);
			$lname = strtoupper($_POST['lname']);
			$empno = strtoupper($_POST['empno']);
			$phone = $_POST['phone'];
			$email = strtoupper($_POST['email']);
			$email_hash = NTLMHash($email);
			$dept = strtoupper($_POST['dept']);
			$division = strtoupper($_POST['division']);
			$region = strtoupper($_POST['region']);
			$access = strtoupper($_POST['access']);

			//check if email exists
			if(isExists($tbl_users, $email))
			{
				//work on this output further
				die("<p class='err_box'>That Email Is Already registered</p>");
			}


			//continue if no user account associated with email

			//email link
			$link = "http://192.168.64.2/imis/newuser.php?sessid=$email_hash";

			$mailer = new PHPMailer();
			$mailer->IsSMTP();
			$mailer->IsHTML();
			$mailer->Host = 'ssl://smtp.gmail.com:465';
			$mailer->SMTPAuth = TRUE;
			$mailer->Username = 'vplangat@gmail.com';  // Change this to your gmail adress
			$mailer->Password = 'B1sc@rd@P@ssw0rd';  // Change this to your gmail password
			$mailer->From = 'vplangat@gmail.com';  // This HAVE TO be your gmail adress
			$mailer->FromName = 'Vincent Kiplangat'; // This is the from name in the email, you can put anything you like here
			
			$msg = "

				<h1 style='width: 90%; background: skyblue; color: white; margin-left: auto; margin-right: auto; text-align: center; padding: 5px 0 5px 0; font-size: 1.5em; font-weight: bolder; text-transform: uppercase;'>
					Ministry Of Mining And Petroleum
					<br/>
					State Deparment of Mining
				</h1>
				<p style='width: 90%; margin-left: auto; margin-right: auto; margin-top: -17px; font-size: 1.0em; padding: 5px 0 5px 0;'>
					Hello <b>$fname $lname</b>,<br/><br/>
					Your Account has been successfully created. Follow this link $link within 24 Hours in order to activate your account. <br/><br/>Your Email will be retrieved automatically by the system, copy and paste the password in the old password field and set new password for your account.
						<br/><br/><b><u>Credentials:-</u></b><br/>
						Username: $email <br/>
						Password: $pass<br/>
						Thank you for signing up. Please <a href='mailto:vplangat@gmail.com'>contact us</a> in case of any problem.
				</p>
				<hr style='width: 90%'/>
				<p style='width: 90%; margin-left: auto; margin-right: auto; margin-top: -17px; font-size: 1.0em; padding: 5px 0 5px 0;'>
					You received this mandatory email because you requested to create an Account for IMIMRS Portal. If You did not authorize this, just ignore this mail.
				</p>
				<hr style='width: 90%;'/>
				<p style='width: 90%; background: skyblue; color: white; margin-left: auto; margin-right: auto; margin-top: -17px; padding: 5px 0 5px 0;'>

					Copyright &copy; 2018. All Rights Are Reserved by IMIRMS<br/>
					Designed by PDTP Cohort III, State Department of Mining
				</p>
			";

			$mailer->Body = $msg;
			$mailer->Subject = "Welcome To IMIRMS Portal, $lname";
			$mailer->AddAddress($email); 

			if(!$mailer->Send())
			{
			   echo "<script>alert('Registration failed. Could not connect to SMTP host. Check Your Internet Connection')</script>";

			}
			else
			{
				$date = date("Y-m-d H:i:s");
				$insert_record = "INSERT INTO $tbl_users VALUES('', '$fname', '$lname', '$empno', '$phone','$email', '$email_hash', '$pass_hash', '$dept', '$division', '$region', '$access', '0', '$added_by', '$date')";
				$is_mail_sent = mysqli_query($con, $insert_record) or die("Error Creating User Account".mysqli_error($con));
				  if($is_mail_sent)
				  {
				  	//reset form fields
				  	$fname = '';
					$lname = '';
					$empno = '';
					$phone = '';
					$email = '';
					$dept = '';
					$division = '';
					$region = '';
					$access = '';
					echo "<script>alert('Registration Successful! Account Details Have Been Sent To The User Email.')</script>";
				}


		}
	}


		echo 
		"
<body>
	<p style='text-align: center;' id='reg_form'>
		<img class='logo' src='images/mining.png'>
	</p>
	<form style='text-align: center;' class='reg_form' action='$server' method='post'>
			<p>REGISTRATION FOR IMIRMS</p>
			<p><input type='text' autofocus title='Please Enter Valid Name e.g John' pattern='^[A-Za-z\s.]+$' name='fname' required placeholder='First Name' ></p>
			<p><input type='text' title='Please Enter Valid Name e.g Doe' pattern='^[A-Za-z\s.]+$' name='lname' required placeholder='Last Name' ></p>
			<p>
				<input type='text' name='empno' placeholder='Employee No.' required>
			</p>
			<p>
				<input type='text' pattern='^[0-9]{10}' name='phone' title='Enter Valid Phone No. e.g 0716826814' placeholder='Mobile No.' required>
			</p>
			<p><input type='text' title='Please Enter Valid Email e.g johndoe@domain.com' pattern='[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$' name='email' required placeholder='Email' ></p>
			<p>
				<select id='groups' name='dept'>
					<option>--> Select Department </option>
				    <option value='Corporate Affairs'>Corporate Affairs</option>
				    <option value='Human Resource'>Human Resource</option>
				    <option value='Geological Surveys'>Geological Surveys</option>
				    <option value='Mines'>Mines</option>
				    <option value='MPVA'>MPVA</option>
				    <option value='DRSRS'>DRSRS</option>
				<select>
			</p>
			<p>
				<select id='sub_groups' name='division'>
				    <option data-group='SHOW' value='0'>--> Select Division </option>
				    <option data-group='Corporate Affairs' value='Administration'>Administration</option>
				    <option data-group='Corporate Affairs' value='ICT'>ICT</option>
				    <option data-group='Corporate Affairs' value='Audit'>Audit</option>
				    <option data-group='Corporate Affairs' value='Records'>Records</option>
				    <option data-group='Corporate Affairs' value='Legal'>Legal</option>
				    <option data-group='Human Resource' value='MHRMAC'>MHRMAC</option>
				    <option data-group='Human Resource' value='IPDP'>IPDP</option>
				    <option data-group='Human Resource' value='Pensions'>Pensions</option>
				    <option data-group='Human Resource' value='Complement'>Complement</option>
				    <option data-group='Human Resource' value='Training'>Training</option>
				    <option data-group='Geological Surveys' value='Exploration'>Exploration</option>
				    <option data-group='Geological Surveys' value='Mapping'>Mapping</option>
				    <option data-group='Geological Surveys' value='Seismology'>Seismology</option>
				    <option data-group='Geological Surveys' value='Data Management'>Data Management</option>
				    <option data-group='Geological Surveys' value='Geodata'>Geodata</option>
				    <option data-group='Mines' value='Mineral Licensing'>Mineral Licensing</option>
				    <option data-group='Mines' value='Mineral Audit'>Mineral Audit</option>
				    <option data-group='Mines' value='Mineral Inspectorate'>Mineral Inspectorate</option>
				    <option data-group='Mines' value='Explosive Inspectorate'>Explosive Inspectorate</option>
				    <option data-group='MPVA' value='Mineral Promotion & Marketing'>Mineral Promotion & Marketing</option>
				    <option data-group='MPVA' value='Mineral Value Addition'>Mineral Value Addition</option>
				    <option data-group='MPVA' value='Mineral Industry Coordination'>Mineral Industry Coordination</option>
				    
				<select>
			</p>
			<p><select name='region'>
				<option selected='selected'>--> Select Region</option>
				<option value='Nairobi'>Nairobi</option>
				<option value='Embu'>Embu</option>
				<option value='Taita Taveta'>Taita Taveta</option>
				<option value='Baringo'>Baringo</option>
				<option value='Garissa'>Garissa</option>
				<option value='Nakuru'>Nakuru</option>
				<option value='Eldoret'>Eldoret</option>
				<option value='Migori'>Migori</option>
				<option value='Kisumu'>Kisumu</option>
				<option value='Vihiga'>Vihiga</option>
				<option value='Kakamega'>Kakamega</option>
				<option value='Malindi'>Malindi</option>
				<option value='Voi'>Voi</option>
				<option value='Meru'>Meru</option>
				<option value='Kwale'>Kwale</option>
			</select></p>
			<p><select name='access'>
				<option selected='selected'>--> Select Access</option>
				<option>Super Admin</option>
				<option>Administrator</option>
				<option>Regional Director</option>
				<option>Normal User</option>
			</select></p>
			<p>
				<input type='submit' name='submit' value='Register'>
			</p>
	</form>
</body>
</html>

		";

	}
	else
		echo "<p class='err_box'>Access To This Page is Restricted!</p>";

?>
