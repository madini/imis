<?php 
	//variables
	$host='localhost';
	$db = 'mining';
	$tbl_users = 'users';
	$tbl_attempts = 'ip';
	$uname = 'root';
	$pass = '';
	//Sql for setting up environment
	$create_db_sql = "CREATE DATABASE IF NOT EXISTS $db";
	$create_tbl_user_sql = "CREATE TABLE IF NOT EXISTS $tbl_users(
	uid INT(4) NOT NULL AUTO_INCREMENT,
	fname TEXT(50) NOT NULL,
	lname TEXT(50) NOT NULL,
	empid VARCHAR(20) NOT NULL,
	phone INT(10) NOT NULL,
	email VARCHAR(100) NOT NULL UNIQUE,
	email_hash VARCHAR(100) NOT NULL,
	pass VARCHAR(100) NOT NULL,
	dept TEXT(100) NOT NULL,
	division TEXT(100) NOT NULL,
	region TEXT(100) NOT NULL,
	access TEXT(50) NOT NULL,
	status INT(1) NOT NULL DEFAULT '1',
	added_by VARCHAR(100) NOT NULL,
	stamp DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY(uid)
)";

	$create_tbl_attempts_sql = "CREATE TABLE IF NOT EXISTS $tbl_attempts(
  	`address` char(16) COLLATE utf8_bin NOT NULL,
  	`timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 	`email` VARCHAR(100) NOT NULL
	) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";
	//connect to server
	$con = new mysqli($host, $uname, $pass);
	if($con->connect_error)
	{
		die("Connection failed: " . $conn->connect_error);
	}
	//create database
	mysqli_query($con, $create_db_sql) or die("Error Creating $db Database");
	//select db
	mysqli_select_db($con, $db) or die("$db Does Not Exist");
	//create table users
	if ($con->query($create_tbl_user_sql) === TRUE) {
    	//table created successfully
	}else
	{
		die("Error creating $tbl_users: " . $con->error);
	}
	
	//create table attempts
	if ($con->query($create_tbl_attempts_sql) === TRUE) {
    	//table created successfully
	}else
	{
		die("Error creating $tbl_attempts: " . $con->error);
	}
	//important functions

	//check if user account exists
	  function isExists($table, $option)
	  {
	  	global $con;
	    $sql = "SELECT * FROM $table WHERE email = '$option'";
	    $query = mysqli_query($con, $sql);
	    if(mysqli_num_rows($query) != 0)
	    {
	      return true;
	    }
	  }

	  //fetch data
	  function fetchData($table, $field, $option)
	  {
	  	global $con;
	    $sql = "SELECT * FROM $table WHERE $field = '$option'";
	    $query = mysqli_query($con, $sql);
	    if(mysqli_num_rows($query) != 0)
	    {
	      $data = mysqli_fetch_array($query);
	      return $data;
	    }else
	    {
	    	return "No Record";
	    }
	  }

	  //check user account data two options
	  function checkData($table, $field1, $field2, $option1, $option2)
	  {
	  	global $con;
	    $sql = "SELECT * FROM $table WHERE $field1 = '$option1' AND $field2='$option2'";
	    $result = mysqli_query($con, $sql);
	    if($result)
	    {
	      return true;
	    }
	  }


	  //Activate user
	  function activateUser($table, $field1, $field2, $field3, $option1, $option2, $option3)
	  {
	  	global $con;
	  	$sql = "UPDATE '$table' SET $field1 ='$option1', $field2 ='$option2', WHERE $field3 ='$option3'";
	  	if(mysqli_query($con, $sql))
	  	{
	  		return true;
	  	}

	  }

	  //Function to  create a default user super admin
	  function defaultUser()
	  {
	  	global $tbl_users, $con;
	  	$sql = "INSERT INTO $tbl_users VALUES('', 'Super', 'User', 'SUP12345', '0716826814', 'VPLANGAT@GMAIL.COM', '36F11EB7AADB71E95192DC6B7C80FD4F', 'E19CCF75EE54E06B06A5907AF13CEF42', 'CORPORATE AFFAIRS', 'ICT', 'NAIROBI', 'SUPER ADMIN', '1', 'VPLANGAT@GMAIL.COM', CURRENT_TIMESTAMP)";
	  	mysqli_query($con, $sql) or die("Error creating default user".mysqli_error($con));
	  }
	  
	  //defaultUser();
?>