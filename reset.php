<?php

	session_start();

	//import dependant files
	require_once("setup.php");
	require_once("functions.php");
	require_once("phpmailer/class.phpmailer.php");
	$_SESSION['superadmin'] = 'VPLANGAT@GMAIL.COM';
	//Variables
	$server = $_SERVER['PHP_SELF'];
	$email = '';
	$pass = randomString();
	$pass_hash = NTLMHash($pass);
	
	echo 
	"
		<!DOCTYPE html>
		<html>
		<head>
			<title>Reset Password - IMIRMS</title>
			<meta name='viewport' content='width=device-width, initial-scale=1'>
			<script src='jquery.js'></script>
			<script type='text/javascript' src='script.js'></script>
			<link rel='stylesheet' type='text/css' href='style.css'>
		</head>
	";

		//check if form is submitted
		if(isset($_POST['submit']))
		{
			$email = strtoupper($_POST['email']);
			$email_hash = NTLMHash($email);
			//check if email exists
			
			$link = "http://192.168.64.2/imis/verify.php?sessid=$email_hash&&auth=$pass";
			$mailer = new PHPMailer();
			$mailer->IsSMTP();
			$mailer->IsHTML();
			$mailer->Host = 'ssl://smtp.gmail.com:465';
			$mailer->SMTPAuth = TRUE;
			$mailer->Username = 'vplangat@gmail.com';  // Change this to your gmail adress
			$mailer->Password = 'B1sc@rd@P@ssw0rd';  // Change this to your gmail password
			$mailer->From = 'vplangat@gmail.com';  // This HAVE TO be your gmail adress
			$mailer->FromName = 'Vincent Kiplangat'; // This is the from name in the email, you can put anything you like here
			
			$msg = "

				<h1 style='width: 90%; background: skyblue; color: white; margin-left: auto; margin-right: auto; text-align: center; padding: 5px 0 5px 0; font-size: 1.5em; font-weight: bolder; text-transform: uppercase;'>
					Ministry Of Mining And Petroleum
					<br/>
					State Deparment of Mining
				</h1>
				<p style='width: 90%; margin-left: auto; margin-right: auto; margin-top: -17px; font-size: 1.0em; padding: 5px 0 5px 0;'>
					You received this mandatory email because you requested to reset password for your IMIRMS Account. If You did not authorize this, just ignore this email.<br/><br/>
					Otherwise, use the following recovery link $link.<br/><br/>
					Kindly change your password after successful login.<br/><br/>
					<b>Credentials:-</b><br/><br/>
					Password: $pass
				</p>
				<hr style='width: 90%;'/>
				<p style='width: 90%; background: skyblue; color: white; margin-left: auto; margin-right: auto; margin-top: -17px; padding: 5px 0 5px 0;'>

					Copyright &copy; 2018. All Rights Are Reserved by IMIRMS<br/>
					Designed by PDTP Cohort III, State Department of Mining
				</p>
			";

			$mailer->Body = $msg;
			$mailer->Subject = "Password Reset: MIRMS Portal";
			$mailer->AddAddress($email); 

			if(!$mailer->Send())
			{
			   echo "<script>alert('Could not connect to SMTP host. Check Your Internet Connection')</script>";
			   
			}
			else
			{
					echo "<script>alert('Password reset link has been send to your email. The link will expire after 2 hours only')</script>";
			}
	}


		echo 
		"
<body>
	<p style='text-align: center;' id='reg_form'>
		<img class='logo' src='images/mining.png'>
	</p>
	<form style='text-align: center;' class='reg_form' action='$server' method='post'>
			<p>PLEASE ENTER YOUR EMAIL TO RESET PASSWORD</p>
			<p><input type='text' title='Please Enter Valid Email e.g johndoe@domain.com' pattern='[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$' name='email' required placeholder='Email' ></p>
			<p>
				<input type='submit' name='submit' value='Reset Password'>
			</p>
	</form>
</body>
</html>

		";

?>
