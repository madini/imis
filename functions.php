<?php
  
  //Hashing credentials
  function NTLMHash($Input) {
    // Convert the password from UTF8 to UTF16 (little endian)
    $Input=iconv('UTF-8','UTF-16LE',$Input);

    // Encrypt it with the MD4 hash
    //$MD4Hash=bin2hex(mhash(MHASH_MD4,$Input));

    // You could use this instead, but mhash works on PHP 4 and 5 or above
    // The hash function only works on 5 or above
    $MD4Hash=hash('md4',$Input);

    // Make it uppercase, not necessary, but it's common to do so with NTLM hashes
    $NTLMHash=strtoupper($MD4Hash);
    // Return the result
    return($NTLMHash);
  }
  
  
  //password generation
  function randomString($length = 8) {
   $str = "";
   $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
   $max = count($characters) - 1;
     for ($i = 0; $i < $length; $i++) {
      $rand = mt_rand(0, $max);
      $str .= $characters[$rand];
     }
   return $str;
  }

  //display header
  function getHeader()
  {
    echo 
    "
      <!DOCTYPE html>
      <html>
      <head>
        <title>Super Admin Dashboard</title>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <script src='jquery.js'></script>
        <script type='text/javascript' src='script.js'></script>
        <link rel='stylesheet' type='text/css' href='style.css'>
      </head>
      <body>";
  }
?>

