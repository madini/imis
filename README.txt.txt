Hello Everyone, I hope this will help us get started and interract even more

Here's is the simple guide to access system
Prerequisite:
You need to have XAMPP or WAMPP server installed and text editor (Sublime Text is cool)

1. Get the repo fro git or bitbucket
2. Extract where necessary
3. Copy imis folder into root dir(htdocs)
4. Open browser and type <ur ip>/imis to access all files
5. Run setup.php to initialize environment once
6. Open setup.php and comment defaultUser() function call
7. You can try to access register.php but you'll be redirected to login page or given security warning since you've not logged in
8. Open login.php and use the creds below:
Username: vplangat@gmail.com
Password: P@ssw0rd 
You'll be redirected to Super Admin page(Presumably your dashboard)
9. Go back to step 7 and you can now register a new user using a valid email, if you have internet connection, you should receive an email notification.
10. Copy the url and paste it in your browser, modify ip address to your own
11. Proceed from there...
12. Let me know of any changes as I continue with the next sections

NB:
On my side I am a bit slower since we have many projects that needs to be delivered on time, I hardly get time to look at our system currently. But we shall finish it on time

Thanks for participating and feel free to share your work with us